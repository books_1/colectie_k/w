# W

## Content

```
./W. Bruce Cameron:
W. Bruce Cameron - Cainele, adevaratul meu prieten 1.0 '{Literatura}.docx

./W. E. Johns:
W. E. Johns - Pe cerul Frantei (fragment) 1.0 '{SF}.docx

./W. Jephson:
W. Jephson - Ochiul zeului Ma-Djuh 1.0 '{Aventura}.docx

./W. W. Jacobs:
W. W. Jacobs - Laba de maimuta 0.9 '{Diverse}.docx

./Wallace D. Wattles:
Wallace D. Wattles - Stiinta de a deveni bogat 0.9 '{DezvoltarePersonala}.docx

./Walter Jon Williams:
Walter Jon Williams - Caderea imperiului - V1 Praxis 2.0 '{SF}.docx
Walter Jon Williams - Dinozaurii 0.9 '{SF}.docx
Walter Jon Williams - Investitii 1.0 '{SF}.docx
Walter Jon Williams - Plafon 0.99 '{SF}.docx
Walter Jon Williams - Sara lucreaza cu Nevastuica 0.99 '{SF}.docx
Walter Jon Williams - Supercablat 1.0 '{SF}.docx

./Walter Krauss:
Walter Krauss - Civilizatia asiro-babiloniana 0.9 '{Istorie}.docx

./Walter Kubilius:
Walter Kubilius - Dincolo 0.99 '{SF}.docx

./Walter Lord:
Walter Lord - Pearl Harbor 1.0 '{ActiuneRazboi}.docx
Walter Lord - Victoria de la Midway 1.0 '{Razboi}.docx

./Walter Miller:
Walter Miller - Cantica pentru Leibowitz 1.0 '{SF}.docx

./Walter Puschel:
Walter Puschel - Robin si fiica sefului de trib 1.0 '{Western}.docx

./Walter Scott:
Walter Scott - Capitanul Waverley 0.7 '{ClasicSt}.docx
Walter Scott - Ivanhoe 5.0 '{ClasicSt}.docx
Walter Scott - Kenilworth 1.0 '{ClasicSt}.docx
Walter Scott - Mireasa din Lammermoor 1.0 '{ClasicSt}.docx
Walter Scott - Quentin Durward 1.0 '{ClasicSt}.docx
Walter Scott - Rob Roy 1.0 '{ClasicSt}.docx
Walter Scott - Talismanul 3.0 '{ClasicSt}.docx

./Walter Scott Elliot:
Walter Scott Elliot - Marirea si decaderea Atlantidei. Lemuria pierduta 1.0 '{Civilizatii}.docx

./Walter Trobisch:
Walter Trobisch - M-am casatorit cu tine 0.99 '{Diverse}.docx

./Warren Veenman:
Warren Veenman - Descatuseaza-ti intregul potential 1.0 '{DezvoltarePersonala}.docx

./Warwick Deeping:
Warwick Deeping - Sorell si fiul 1.0 '{Literatura}.docx

./Washington Irving:
Washington Irving - Aventura Unui Student German 0.9 '{Literatura}.docx
Washington Irving - O diligenta medievala 1.0 '{Literatura}.docx

./Wayland Smith:
Wayland Smith - Ancheta 1.0 '{SF}.docx

./Web Griffin:
Web Griffin - Din ordinul presedintelui 1.0 '{ActiuneComando}.docx
Web Griffin - Ostaticul V1 1.0 '{ActiuneComando}.docx
Web Griffin - Ostaticul V2 1.0 '{ActiuneComando}.docx

./Werner Keller:
Werner Keller - Si totusi Biblia are dreptate 1.0 '{MistersiStiinta}.docx

./Werner Steinberg:
Werner Steinberg - Palaria comisarului 1.0 '{Politista}.docx

./Weston Ochse:
Weston Ochse - Onoarea lui Yautja 1.0 '{SF}.docx

./Whitley Strieber:
Whitley Strieber - Zona interzisa 2.0 '{Horror}.docx

./Wieslaw Kielarwk:
Wieslaw Kielarwk - 5 ani la Auschwitz 1.1 '{Razboi}.docx

./Wieslaw Mysliwski:
Wieslaw Mysliwski - Orizont 1.0 '{Literatura}.docx
Wieslaw Mysliwski - Piatra peste piatra 3.0 '{Literatura}.docx

./Wilbur Smith:
Wilbur Smith - Egiptul Antic - V1 Razboinicii Nilului 1.5 '{AventuraIstorica}.docx
Wilbur Smith - Egiptul Antic - V2 Magul 1.5 '{AventuraIstorica}.docx
Wilbur Smith - Egiptul Antic - V3 Ultimul papirus 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Egiptul Antic - V4 Cautarea 1.0 '{AventuraIstorica}.docx
Wilbur Smith - In pericol 0.8 '{Literatura}.docx
Wilbur Smith - Saga familiei Courtney - V1 Festinul leilor 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V2 Sunetul tunetului 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V3 Sanctuarul 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V4 Tarmul in flacari 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V5 Puterea sabiei 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V6 Furia 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V7 Vulpea aurie 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V8 Vremea mortii 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V9 Pasari de prada 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V10 Musonul 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V11 Orizontul albastru 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V12 Triumful Soarelui 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Saga familiei Courtney - V13 Assegai 1.0 '{AventuraIstorica}.docx
Wilbur Smith - Zeul desertului 1.0 '{AventuraIstorica}.docx

./Wilhelm Busch:
Wilhelm Busch - Cand omul nu mai poate sa creada 0.9 '{Religie}.docx
Wilhelm Busch - Cand va fi sfarsitul lumii 0.9 '{Religie}.docx
Wilhelm Busch - Dreptul nostru la dragoste 0.8 '{Religie}.docx
Wilhelm Busch - Pentru ce exist pe acest pamant 0.9 '{Religie}.docx

./Wilhelm Hauff:
Wilhelm Hauff - Basme 1.0 '{BasmesiPovesti}.docx
Wilhelm Hauff - Muc cel mic 0.9 '{BasmesiPovesti}.docx
Wilhelm Hauff - Povestea vasului fantoma 0.99 '{BasmesiPovesti}.docx

./Wilhelm Raabe:
Wilhelm Raabe - Cronica ulitei vrabiilor 1.0 '{Literatura}.docx

./Wiliam Austin:
Wiliam Austin - Peter Rugg, disparutul 0.99 '{Diverse}.docx

./Wilkie Collins:
Wilkie Collins - Femeia in alb 0.7 '{Diverse}.docx
Wilkie Collins - Piatra lunii 1.0 '{Aventura}.docx

./Will Adams:
Will Adams - Knox - V1 Codul lui Alexandru 1.0 '{AventuraIstorica}.docx

./Willard Flemming:
Willard Flemming - Arnold Schwarzenegger 1.9 '{Biografie}.docx

./Willa Roberts:
Willa Roberts - O seara pentru noi 0.2 '{Romance}.docx

./Will Garth:
Will Garth - Doctorul Ciclop 1.0 '{Aventura}.docx

./William B. Miller:
William B. Miller - Joe's garage 0.9 '{Biografie}.docx

./William Boyd:
William Boyd - Cu ochii-n patru 1.0 '{Literatura}.docx

./William Davis:
William Davis - Dieta fara gluten 1.0 '{Sanatate}.docx

./William Diehl:
William Diehl - 27 1.0 '{Thriller}.docx
William Diehl - Calul thailandez 1.0 '{Thriller}.docx
William Diehl - Cameleonul 2.0 '{Thriller}.docx
William Diehl - Chipul raului 0.99 '{Thriller}.docx
William Diehl - Domnie in iad 0.99 '{Thriller}.docx
William Diehl - Fiinta raului 0.99 '{Thriller}.docx
William Diehl - Huliganii 1.0 '{Thriller}.docx

./William F. Temple:
William F. Temple - Masina verde 0.99 '{SF}.docx

./William Faulkner:
William Faulkner - Absalom, Absalom! 2.0 '{Thriller}.docx
William Faulkner - Gambitul calului 1.0 '{Thriller}.docx
William Faulkner - Hotomanii 2.0 '{Thriller}.docx
William Faulkner - Lumina de august 1.0 '{Thriller}.docx
William Faulkner - Nechemat in tarana 1.0 '{Thriller}.docx
William Faulkner - Neinfrantii 1.0 '{Thriller}.docx
William Faulkner - Pe patul de moarte 5.0 '{Thriller}.docx
William Faulkner - Pogoara-te, Moise 1.0 '{Thriller}.docx
William Faulkner - Recviem pentru o calugarita 1.0 '{Thriller}.docx
William Faulkner - Sanctuar 1.0 '{Thriller}.docx
William Faulkner - Sartoris 2.0 '{Thriller}.docx
William Faulkner - Snopes - V1 Catunul 2.0 '{Thriller}.docx
William Faulkner - Snopes - V2 Orasul 2.0 '{Thriller}.docx
William Faulkner - Snopes - V3 Casa cu coloane 2.0 '{Thriller}.docx
William Faulkner - Ursul 1.0 '{Thriller}.docx
William Faulkner - Zgomotul si furia 2.0 '{Thriller}.docx

./William Gibson:
William Gibson - Camera lui Skinner 1.0 '{SF}.docx
William Gibson - Chrome 1.0 '{SF}.docx
William Gibson - Contele zero 1.0 '{SF}.docx
William Gibson - Continuumul Gernsback 1.0 '{SF}.docx
William Gibson - Fragmente din holograma unui trandafir 1.0 '{SF}.docx
William Gibson - Hotelul trandafirul nou 1.0 '{SF}.docx
William Gibson - Johnny Mnemonic 1.0 '{SF}.docx
William Gibson - Lumina virtuala 1.0 '{SF}.docx
William Gibson - Machina diferentiala 1.0 '{SF}.docx
William Gibson - Neuromantul 2.0 '{SF}.docx
William Gibson - Piata in iarna 1.0 '{SF}.docx
William Gibson - Zona de frontiera 1.0 '{SF}.docx

./William Gibson & Bruce Sterling:
William Gibson & Bruce Sterling - Stea rosie pe orbita de iarna 1.0 '{SF}.docx

./William Gibson & John Shirley:
William Gibson & John Shirley - Specie integrata 1.0 '{SF}.docx

./William Gibson & Michael Swanwick:
William Gibson & Michael Swanwick - Lupta aeriana 1.0 '{SF}.docx

./William Golding:
William Golding - Dublu limbaj 0.9 '{Literatura}.docx
William Golding - Imparatul mustelor 1.1 '{Literatura}.docx
William Golding - Infruntarea 0.9 '{Literatura}.docx
William Golding - Initierea 0.9 '{Literatura}.docx
William Golding - Zeul scorpion 0.7 '{Literatura}.docx

./William Gordon:
William Gordon - Vasele chinezesti 1.0 '{Literatura}.docx

./William H. Hodgson:
William H. Hodgson - Casa de la marginea abisului 1.0 '{SF}.docx

./William H. McRaven:
William H. McRaven - Fa-ti patul 1.0 '{DezvoltarePersonala}.docx

./William Henry Hudson:
William Henry Hudson - Palatele verzi 0.99 '{Literatura}.docx

./William Howells:
William Howells - Ascensiunea lui Silas Lapham V1 0.9 '{Literatura}.docx
William Howells - Ascensiunea lui Silas Lapham V2 0.9 '{Literatura}.docx
William Howells - O cunostinta intamplatoare 0.9 '{Literatura}.docx
William Howells - Stapanul de la Capul Leului 0.9 '{Literatura}.docx
William Howells - Un caz modern 0.9 '{Literatura}.docx
William Howells - Vara indiana 1.0 '{Literatura}.docx

./William King:
William King - Protectorul 1.0 '{SF}.docx

./William Kotzwinkle:
William Kotzwinkle - E.T. extraterestrul 2.0 '{SF}.docx

./William Landay:
William Landay - In apararea lui Jacob 1.0 '{Literatura}.docx

./William Lee:
William Lee - Jocuri de copii 1.0 '{SF}.docx

./William Lewis Manly:
William Lewis Manly - Valea mortii 1.0 '{Western}.docx

./William Lovejoy:
William Lovejoy - Batalie Aeriana 1.0 '{ActiuneComando}.docx

./William Macdonald:
William Macdonald - Cum sa intelegem corect Biblia 0.9 '{Religie}.docx

./William Manchester:
William Manchester - Adio, intuneric - Memorii din razboiul Pacificului 1.0 '{Razboi}.docx
William Manchester - Armele lui Krupp 1.0 '{Istorie}.docx

./William P. Sanders:
William P. Sanders - Accidentul OZN - V1 Accidentul OZN 1.0 '{MistersiStiinta}.docx
William P. Sanders - Accidentul OZN - V2 Prabusirea in viitor 1.0 '{MistersiStiinta}.docx

./William Peter Blatty:
William Peter Blatty - Dimiter 1.0 '{Suspans}.docx
William Peter Blatty - Exorcistul 1.0 '{Horror}.docx

./William Shakespeare:
William Shakespeare - Opere complete V1 1.0 '{ClasicSt}.docx
William Shakespeare - Opere complete V2 1.0 '{ClasicSt}.docx
William Shakespeare - Opere complete V3 1.0 '{ClasicSt}.docx
William Shakespeare - Opere complete V4 1.0 '{ClasicSt}.docx
William Shakespeare - Opere complete V5 1.0 '{ClasicSt}.docx
William Shakespeare - Opere complete V6 1.0 '{ClasicSt}.docx
William Shakespeare - Opere complete V7 1.0 '{ClasicSt}.docx
William Shakespeare - Opere complete V8 1.0 '{ClasicSt}.docx
William Shakespeare - Opere complete V9 1.0 '{ClasicSt}.docx

./William Somerset Maugham:
William Somerset Maugham - A tremurat o frunza 1.0 '{Literatura}.docx
William Somerset Maugham - Doamna Craddock 1.0 '{Dragoste}.docx
William Somerset Maugham - Julia 1.0 '{Literatura}.docx
William Somerset Maugham - Luna si doi bani jumate 1.0 '{Literatura}.docx
William Somerset Maugham - Robie 1.0 '{Literatura}.docx
William Somerset Maugham - Vacanta de Craciun 1.0 '{Literatura}.docx
William Somerset Maugham - Valul pictat 1.0 '{Literatura}.docx

./William Stevenson:
William Stevenson - I se spunea temerarul 1.0 '{Istorie}.docx

./William Styron:
William Styron - Dati foc acestei case 0.9 '{Literatura}.docx

./William Tenn:
William Tenn - Bernie the Faust 1.0 '{Diverse}.docx

./William Thackeray:
William Thackeray - Balciul desertaciunilor V1 0.9 '{Dragoste}.docx
William Thackeray - Balciul desertaciunilor V2 0.9 '{Dragoste}.docx

./William Trevor:
William Trevor - Calatoria Feliciei 1.0 '{Literatura}.docx
William Trevor - Povestea lui Lucy Gault 0.9 '{Literatura}.docx

./William Yeats:
William Yeats - Rosa Alchemica si alte scrieri 0.99 '{Literatura}.docx

./Willi Meinck:
Willi Meinck - Uluitoarele aventuri ale lui Marco Polo V1 1.0 '{Aventura}.docx
Willi Meinck - Uluitoarele aventuri ale lui Marco Polo V2 1.0 '{Aventura}.docx

./Wilson Tucker:
Wilson Tucker - Expuneri temporale 0.99 '{CalatorieinTimp}.docx

./Winston Churchill:
Winston Churchill - Al doilea razboi mondial V1 1.0 '{Istorie}.docx
Winston Churchill - Al doilea razboi mondial V2 0.9 '{Istorie}.docx

./Winston Graham:
Winston Graham - Poldark - V1 Ross Poldark 3.0 '{Literatura}.docx
Winston Graham - Poldark - V2 Demelza 4.0 '{Literatura}.docx
Winston Graham - Poldark - V3 Zile zbuciumate 4.0 '{Literatura}.docx
Winston Graham - Poldark - V4 Warleggan 2.0 '{Literatura}.docx
Winston Graham - Poldark - V5 Luna neagra 2.0 '{Literatura}.docx
Winston Graham - Poldark - V6 Cele patru lebede 2.0 '{Literatura}.docx
Winston Graham - Poldark - V7 Valul furios 2.0 '{Literatura}.docx
Winston Graham - Poldark - V8 Strainul de pe mare 2.0 '{Literatura}.docx

./Winston Groom:
Winston Groom - Forrest Gump 1.0 '{Literatura}.docx

./Wladyslaw Reymont:
Wladyslaw Reymont - Comedianta. Framantari 1.0 '{Literatura}.docx
Wladyslaw Reymont - Fiu de nobili 2.0 '{ClasicSt}.docx

./Wolfgang Hohlbein:
Wolfgang Hohlbein - Anubis 1.0 '{SF}.docx
Wolfgang Hohlbein - Charity, o femeie in Space Force 2.0 '{SF}.docx
Wolfgang Hohlbein - Inchizitorul 1.0 '{AventuraIstorica}.docx

./Woody Allen:
Woody Allen - Anarhie pura 1.0 '{Diverse}.docx

./Wu Jingzi:
Wu Jingzi - Intamplari din lumea carturarilor V1 1.0 '{Literatura}.docx
Wu Jingzi - Intamplari din lumea carturarilor V2 1.0 '{Literatura}.docx
```

